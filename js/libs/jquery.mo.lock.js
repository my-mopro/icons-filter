/* ============================================
molock

properties
    value
events
    onLock()
    onUnlock()
methods
    val()           // return true/false  
    lock(bFire)     // lock, bFire - true/false to firse event
    unlock(bFire)   // unlock, bFire - true/false to firse event
    destroy()       // destroy plugin
===============================================*/

define("molock", ["jquery"], function (jQuery) {

    ;( function( $, window, document, undefined ) {
	    "use strict";
    
        // Create the defaults once
        var pluginName = "molock",
            defaults = {
                value: false,
                onLock: null,
                onUnlock: null,
                lockHTML: '<i class="fa fa-lock" aria-hidden="true"></i> LOCK',
                unlockHTML: '<i class="fa fa-unlock" aria-hidden="true"></i> UNLOCK',
            };

        // The actual plugin constructor
        function Plugin ( element, options ) {
            this.el = element;
            this.$el = $(this.el);
            this.settings = $.extend( {}, defaults, options );
            this._defaults = defaults;
            this._name = pluginName;
            this.init();
        }

        // Avoid Plugin.prototype conflicts
        $.extend( Plugin.prototype, {
            init: function() {
                var me = this;
                me.settings.value = !!me.settings.value;

                this.updateUI();
                this.bindEvents();
            },
            bindEvents: function() {
                var me = this;

                me.$el.on('click'+'.'+me._name, function() {
                    me.settings.value ? me.unlock.call(me, true) : me.lock.call(me, true);
                });
            },
            unbindEvents: function() {
                this.$el.off('.'+this._name);
            },
            destroy: function() {
                this.unbindEvents();
                this.$el.removeData();
            },
            updateUI: function() {
                this.$el.html( (this.settings.value) ? this.settings.lockHTML : this.settings.unlockHTML );
            },
            val: function() {
                return this.value;
            },
            set: function(bLock) {
                this.settings.value = !!bLock;
                this.updateUI();
            },
            lock: function(bFire) {
                if (bFire && $.isFunction(this.settings.onLock)) {
                    var bReturn = this.settings.onLock.call(this);
                    // pass if return value is true or undefined
                    (bReturn || bReturn === undefined) && this.set(true);
                }
                else {
                    this.set(true);
                }

                return this;
            },
            // bFire = true to fire event
            unlock: function(bFire) {
                if (bFire && $.isFunction(this.settings.onUnlock)) {
                    var bReturn = this.settings.onUnlock.call(this);
                    (bReturn || bReturn === undefined) && this.set(false);
                }
                else {
                    this.set(false);
                }

                return this;
            }
        } );

        // A really lightweight plugin wrapper around the constructor,
        // preventing against multiple instantiations
        $.fn[ pluginName ] = function( options ) {
            return this.each( function() {
                if ( !$.data( this, pluginName ) ) {
                    $.data( this, pluginName, new Plugin( this, options ) );
                }
            } );
        };

    } )( jQuery, window, document );

    return jQuery.fn.molock;
});