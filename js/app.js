'use strict';

//Configure require.js
require.config({
  shim: {
    underscore: {
      exports: '_'
    },
    backbone: {
      deps: [
        'underscore',
        'jquery'
      ],
      exports: 'Backbone'
    }, 
    marionette: {
        deps: [
            'jquery',
            'underscore',
            'backbone',
            'underscore',
            'backbone.radio'
        ],
        exports: 'Mn'
    },
    minicolors: {
        deps: [
            'jquery'
        ],
        exports: 'minicolors'
    },
    selectik: {
        deps: [
            'jquery',
            'mousewheel'
        ],
        exports: 'selectik'
    },
    minimalect: {
        deps: [
            'jquery'
        ],
        exports: 'minimalect'
    },
    molock: {
        deps: [
            'jquery'
        ],
        exports: 'molock'
    },
    mospinner: {
        deps: [
            'jquery'
        ],
        exports: 'mospinner'
    },
    slimscroll: {
        deps: [
            'jquery',
        ],
        exports: 'slimscroll'
    },
  },
  paths: {
      jquery: 'libs/jquery-2.0.3.min',
      underscore: 'libs/underscore-min',
      backbone: 'libs/backbone-min',
      text: 'libs/text',
      'backbone.radio': 'libs/backbone.radio',
      marionette: 'libs/backbone.marionette',
      minicolors: 'libs/jquery.minicolors',
      selectik: 'libs/jquery.selectik',
      mousewheel: 'libs/jquery.mousewheel',
      minimalect: 'libs/jquery.minimalect',
      molock: 'libs/jquery.mo.lock',
      mospinner: 'libs/jquery.mo.spinner',
      tinycolor: 'libs/tinycolor-min',
      rangy: 'libs/rangy-core',
      slimscroll: 'libs/jquery.slimscroll',
      
      // view
      rtechannel: 'view/rteChannel',
      rtelistener: 'view/rteListener',
      rtepanel: 'view/rtePanel'      
  },
  map: {
    '*': {
        'css': 'libs/css.min' // or whatever the path to require-css is
    }
  }
});

//Start up our App
require([
    'marionette',
    'backbone.radio',
    'collection/users',
    'view/app',
    'view/main',
    'view/about',
    'view/detail',
    'css!../css/diy'
], 
function (Mn, Radio, UserCollection, AppView, MainView, AboutView, DetailView) {
    var AppRouter = Mn.AppRouter.extend({
        routes: {
            "": "home",
            "about": "about",
            "contact": "contact",
            "user/:id": "detail"
        },
        initialize: function() {
            this.app = this.options.app;
            this.mainRegion = this.app.getRegion().currentView.getRegion("main");
        },
        home: function() {
            this.mainRegion.show(new MainView());
            
            //console.log("home");
        },
        about: function() {
            this.mainRegion.show(new AboutView());
            
            //console.log("about");
        },
        contact: function() {
            //console.log("contact");
        },
        detail: function(id) {
            this.mainRegion.show(new DetailView());
        }
    });
    
    var App = Mn.Application.extend({
        region: "#appRegion",
        onBeforeStart: function() {
            /*
            var channel = Radio.channel("basic");            
            channel.on("some:event", function(oParam){
                console.log("something happen! - " + oParam.type);
                //channel.off('some:event');
                return "aaaaaa";
            })
            
            var channelNotify = Radio.channel("notify");        
            channelNotify.reply("show:error", this.showError);
            */
        },
        showError: function(msg) {
            console.log("showError - " + msg);  
            return "err-101";
        },
        showError2: function(msg) {
            console.log("showError2 - " + msg);  
            return "err-102";
        },
        onStart: function() {
            this.showView(new AppView());
            this.router = new AppRouter({"app": this});
            Backbone.history.start();
            
            //var channel = Radio.channel("basic");
            
            /*
            this.listenTo(channel, 'some:event', function(oParam){
                console.log("listen to something happen! - " + oParam.type);
            });
            */
            
            /*
            var channelNotify = Radio.channel("notify");        
            channelNotify.reply("show:error", this.showError2);
            */
        }
    });
    
    var app = new App();
    app.start();
});