'use strict';

define([
    'jquery',
    'underscore',
    'marionette',
    'backbone.radio',
    'text!tmpl/header.html'
], function($, _, Mn, Radio, templateHTML){
    var view = Mn.View.extend({
        template: _.template(templateHTML),
        ui: {
            but: "#butSelectAll"
        },
        events: {
            "click @ui.but": "selectAll"
        },
        onRender: function() {
            console.log('header - render');
        },
        onDestroy: function() {
            console.log('header - destroy');
        },
        selectAll: function(event) {
            Radio.channel("rte").request("rte:selectAll");
        }
    });
    
    return view;
});