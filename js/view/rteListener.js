'use strict';

define("rtelistener", ['jquery', 'underscore', 'backbone', 'marionette', 'backbone.radio'], 
function($, _, Bb, Mn, Radio){
    var view = Mn.View.extend({
        template: false,
        events: function() {
            var oRet = {},
                selector = this.options.allowEditSelectors || this.defaultOptions.allowEditSelectors;
            
            // situation - user mousedown on editable element 1 and drag and let go on another editable element 2
            // in this case mouseup on editable won't fire and instead mouseup on editable element 2 will first 

            oRet["mouseup " + selector] = "clickEditable";
            return oRet;
        },
        defaultOptions: {
            // allow edit for certain selectors
            //allowEditSelectors : ".js-aieditor-title, .js-aieditor-subtitle, .js-aieditor-vptitle, .js-aieditor-vpsubtitle"
            allowEditSelectors : ".js-aieditor-title, .js-aieditor-subtitle"
        },
        channelName: "rte",
        radioEvents: {
            "rte:enter": "rteEnter"
        },
        initialize: function(){
            console.log('rte listener - init');
            
            var me = this;
            
            me.options = _.extend({}, me.defaultOptions, me.options);
            
            var channel = Radio.channel("rte");            
            channel.on("rte:enter", function(oParam){                 
                if (oParam.ele === me.rteEle) { 
                    console.log("rte:enter - " + oParam.ele.id);
                }
            })
            .on("rte:leave", function(oParam){                                
                if (oParam.ele === me.rteEle) {        
                    if (oParam.ele) {
                        console.log("rte:leave - " + oParam.ele.id);
                    }                    
                }
            })
            .on("rte:edit", function(oParam){ 
                if (oParam.ele === me.rteEle) {        
                    if (oParam.ele) {
                        //console.log("rte:save - " + oParam.ele.id);
                        //me.$el.trigger("rte:save", {ele: me.rteEle});
                        if ($.isFunction(me.options.onChange)) {
                            me.options.onChange();    
                        }                        
                    }                    
                }                
            })
            .on("rte:removeFormat", function(oParam){ 
                if (oParam.ele === me.rteEle) {        
                    if (oParam.ele) {
                        var range = document.createRange();
                        range.selectNodeContents(oParam.ele);
                        var sel = window.getSelection();
                        sel.removeAllRanges();
                        sel.addRange(range);
                        
                        document.execCommand("removeformat", false, "");    
                        $(".bold, .italic, .underline", oParam.ele).removeClass("bold italic underline");
                        sel.removeAllRanges();
                        
                        if ($.isFunction(me.options.onChange)) {
                            me.options.onChange();    
                        }                        
                    }                    
                }                
            })                
            .on("rte:stopListening", function(oParam){ 
                me.stopListening();
                me.undelegateEvents();
            });
        },        
        onRender: function() {
            console.log('rte listener - render');
        },
        onDestroy: function() {
            console.log('rte listener - destroy');
        },
        clickEditable: function(event) {       
            this.rteEle = event.currentTarget; 
            
            if (this.rteEle.getAttribute("convert")) {
                ;
            }
            else {
                this.rteEle.setAttribute("convert", 1);
                /*
                $(".bold", this.rteEle).wrap("<b class='bold'>").removeClass("bold");
                $(".underline", this.rteEle).wrap("<u class='underline'>").removeClass("underline");
                $(".italic", this.rteEle).wrap("<i class='italic'>").removeClass("italic");
                
                $(".bold", this.rteEle).wrap("<b>").removeClass("bold");
                $(".underline", this.rteEle).wrap("<u>").removeClass("underline");
                $(".italic", this.rteEle).wrap("<i>").removeClass("italic");
                */
            }
        },
        rteEnter: function(oParam) {
            console.log("rte Enter - " + oParam);
        }
    });

    return view;
});
