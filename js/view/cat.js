'use strict';

define(['jquery', 'underscore', 'backbone', 'marionette', 'view/list', 'text!tmpl/cat.html'], 
function($, _, Bb, Mn, ItemList, templateHTML) {
    var v = Mn.View.extend({
        template: _.template("ssss"),
        onRender: function() {
        },
        onDestroy: function() {
            
        }        
    });

    var view = Mn.View.extend({
        template: _.template(templateHTML),        
        regions: {
            list2: ".js_list"
        },
        onRender: function() { 
            this.showChildView("list2", new ItemList({
                collection: this.collection
            }));            
        },
        onDestroy: function() {
            
        }        
    });
        
    return view;
});
