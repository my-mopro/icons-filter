'use strict';

define([
    'jquery',
    'underscore',
    'marionette',
    'backbone.radio',
    'view/subItem',
    'rtelistener'
], function($, _, Mn, Radio, SubItem, RteListener){
    var view = Mn.View.extend({        
        tagName: "li",
        
        template: _.template("<i class='<%=ShortDescr%>' title='<%=Description%>'></i>"),
        
        template2: _.template("<span id='a<%=id%>0' class='js-aieditor-title' contenteditable><%= content %></span>" + 
                             "<div id='a<%=id%>1' class='js-aieditor-subtitle' contenteditable><%= content %></div>" +
                             "<div class='subItem'><div id='a-suv<%=id%>1' class='js-aieditor-vptitle' contenteditable style='border: 1px red solid'><%= content %></div></div>"
        ),
        ui: {
            subItem: ".subItem",
            
        },
        triggers: {
            "click @ui.del": "delete:item"  
        },
        events: {
            "click @ui.del": "deleteModel",
            "rte:save": "saveRte"
        },        
        onRender: function() {    
            var me = this;
            
            /*
            new RteListener({el: this.el,
                onChange: function() {
                    clearTimeout(me.saveTimer);
                    me.saveTimer = setTimeout(function(){  
                        me.saving();
                    }, 3000);
                    
                }
            });
            
            new SubItem({el: this.ui.subItem[0]});
            */
            
            console.log('li - render');                    
        },
        saving: function() {
            console.log("save done!");
        },
        onDestroy: function() {
            console.log('li - destroy');
        },
        deleteModel: function() {
            /*
            var basicChannel = Radio.channel("basic");
            basicChannel.trigger("some:event", {type: "2"});
                        
            var channelNotify = Radio.channel("notify");        
            alert(channelNotify.request("show:error", "this is an error"));
            */
        },
        saveRte: function() {
            console.log("save Rte");
        }
    });
    
    return view;
});