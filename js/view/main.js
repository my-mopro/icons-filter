'use strict';

define(['jquery', 'underscore', 'backbone', 'marionette', 'text!tmpl/main.html', 'view/cat'],
function($, _, Bb, Mn, templateHTML, CatView) {
    var view = Mn.View.extend({
        template: _.template(templateHTML),
        ui: {
            txt: ".txtSearch"  
        },
        events: {
            "click .btn": "click",
            "click .addItem": "addItem",
            "click .removeItem": "removeItem",
            "keyup @ui.txt": _.debounce(function(event) {
                this.filterIcon(event);
            }, 300)
        },
        regions: {
            list: ".listRegion"
        },
        filterIcon: function(event) {
            _.each(this.arrColView, function(val, key, list) {     
                val.getRegion("list2").currentView.setFilter(function (child, index, collection) {                       
                    return child.get('Description').indexOf(event.currentTarget.value) > -1;
                }, { preventRender: true });  
                
                val.getRegion("list2").currentView.render();            
                if (val.getRegion("list2").currentView.el.childNodes.length === 0) {
                    val.$el.hide();
                }
                else {
                    val.$el.show();
                }                
            });        
        },
        onRender: function() {            
            /*
            this.listColView = new ListColView({collection: this.collection});
            this.listColView.render();
            this.$el.append(this.listColView.$el);
            */
            
            var me = this;            
            this.arrColView = [];
            
            var IconCollection = Bb.Collection.extend({
                url: "/js/data/icon.json"
            });
                        
            var colIcon = new IconCollection();
            colIcon.fetch({
                success: function(data){
                    
                    var g = data.groupBy(function (val) {
                        return val.get("LongDescr");
                    });
                    
                    _.each(g, function(val, key, list) {                        
                        var category = key.replace(/\s+/g, "_");
                        me.$el.append("<div class='" + category + "'></div>");
                        me.addRegion(category, "."+category);
                        me.arrColView.push(new CatView({
                            model: new Bb.Model({longDesc: key}),
                            collection: new Bb.Collection(val)
                        }));
                        me.showChildView(category, me.arrColView[me.arrColView.length-1]);            
                    });                        
                }
            });
            
            
            //this.showChildView("list", new ListColView({collection: colIcon}));
                                                        
            console.log('main - render');
        },
        onDestroy: function() {
            console.log('main - destroy');
        },
        click: function(event) {
            console.log("main - click!");
        },
        addItem: function(event) {
            this.collection.add({id: this.collection.length, name: this.ui.txt.val(), age: 30});
        },
        removeItem: function(event) {
            this.collection.pop();
        },
        onChildviewDeleteItem: function(childView) {
            this.collection.remove(childView.model);
        }
    });
    
    return view;
});

