'use strict';

define("rtepanel", [
    'jquery',
    'underscore',
    'backbone',
    'marionette',
    'backbone.radio',
    'mospinner',
    'minicolors',
    'selectik',
    'minimalect',
    'tinycolor',
    'rangy',
    'slimscroll',
    'text!tmpl/rtePanel.html',
], function ($, _, Bb, Mn, Radio, mospinner, minicolors, selectik, minimalect, tinycolor, rangy, slimscroll, templateHTML) {
    var TypeView = Mn.View.extend({
        tagName: "li",
        template: _.template("<%=FontStyleName%>"),
        triggers: {
            "click": {
                event: "change:type",
                stopPropagation: false
            }
        }
    });

    var TypeColView = Mn.CollectionView.extend({
        tagName: "ul",
        className: "font_types",
        childView: TypeView,
        onChildviewChangeType: function (view) {
            this.trigger("change:type", view);
        }
    });

    var FontView = Mn.View.extend({
        tagName: "li",
        className: "font_name",
        template: _.template("<span></span>"),
        triggers: {
            "click": {
                event: "change:font",
                stopPropagation: false
            }
        },
        onRender: function () {
            this.$el.addClass("font-"+this.model.get("ID"));
            if (this.model.get("selected")) {
                this.$el.addClass("selected");
            }
        }
    });

    var FontColView = Mn.CollectionView.extend({
        tagName: "ul",
        className: "font_names",
        childView: FontView,
        ui: {
            back: ".js_back"
        },
        triggers: {
            "click @ui.back": "click:back"
        },
        onChildviewChangeFont: function (view) {
            // remove selected on all li
            this.$el.find("li").removeClass("selected");
            // and add selected to click li
            view.$el.addClass("selected");

            this.trigger("change:font", view);
        },
        filter: function (child, index, collection) {
            return false;
        }
    });

    var view = Mn.View.extend({
        className: "js_rte rte-toolbar",
        template: _.template(templateHTML),
        ui: {
            button: ".js_button",
            bold: ".js_bold",
            italic: ".js_italic",
            underline: ".js_underline",
            size: ".js_selSize",
            textCase: ".js_selTextCase",
            spacing: ".js_selTextSpacing",
            lineHeight: ".js_selLineHeight",
            tab1: ".tab1",
            tab2: ".tab2",
            tab1Content: ".tab1_content",
            tab2Content: ".tab2_content",
            fontWrapper: ".js_font_wrapper",
            setDefault: ".js_default",
            back: ".js_back",
            color: ".js_color"
        },
        events: {
            "click @ui.button": "clickButton",
            "change @ui.textCase": "formatTextCase",
            "click @ui.tab1, @ui.tab2": "switchTab",
            "click @ui.back": "showFont",
            "click @ui.setDefault": "removeFormat",
            "focus input": "inputFocus",
            "blur input": "inputBlur"
        },
        regions: {
            fontType: ".js_font_type_region",
            fontList: ".js_font_list_region"
        },
        initialize: function () {
            this.sFontStyleName = "";
        },
        groupByType: function () {
            return _.map(this.collection.groupBy(function (val) {
                return val.get("FontStyleName");
            }),
                    function (val, key) {
                        return { "FontStyleName": key };
                    }
            );
        },
        onRender: function () {
            console.log('rte panel - render');

            var me = this;

            this.collectionFontType = new Bb.Collection(me.groupByType());
            this.colViewFontType = new TypeColView({ collection: this.collectionFontType });
            this.colViewFont = new FontColView({ collection: this.collection })
            this.showChildView("fontType", this.colViewFontType);
            this.showChildView("fontList", this.colViewFont);

            this.ui.color.minicolors({
                //changeDelay: 0,
                opacity: true,
                change: function (value, opacity) {
                    if (!value) return;

                    // exit if formatTextCase is called from UpdateSettings function
                    if (me.bUpdatingSettings) {
                        return;
                    }
                    
                    if (!me.bInputFocus) {
                        setTimeout(function () {
                            me.restoreSelection();
                            me.formatEle({ type: "css", name: "color", value: tinycolor(value).setAlpha(opacity).toString() });
                        }, 1);
                    }
                },
                theme: 'bootstrap'
            });

            this.ui.textCase.selectik({
                width: 100
            });

            this.ui.size.mospinner({
                min: 10,
                max: 100,
                value: 20,
                delay: 10,
                onChange: function (iValue) {
                    me.restoreSelection();
                    setTimeout(function () {
                        me.formatEle({ type: "css", name: "font-size", value: iValue + "px" });
                    }, 1);
                }
            });

            this.ui.spacing.mospinner({
                min: 0,
                max: 50,
                value: 0,
                delay: 10,
                onChange: function (iValue) {
                    me.restoreSelection();
                    setTimeout(function () {
                        me.formatEle({ type: "css", name: "letter-spacing", value: iValue + "px" });
                    }, 1);
                }
            });

            this.ui.lineHeight.mospinner({
                min: 10,
                max: 200,
                value: 30,
                delay: 10,
                onChange: function (iValue) {
                    me.restoreSelection();
                    setTimeout(function () {
                        me.formatEle({ type: "css", name: "line-height", value: iValue + "px" });
                    }, 1);
                }
            });
            
            this.getRegion("fontType").$el.slimScroll({
                height: "185px"
            });
                        
            this.getRegion("fontList").$el.slimScroll({
                height: "185px"
            });            
        },
        onDestroy: function () {
            console.log('rte panel - destroy');
        },
        inputFocus: function() {
            this.bInputFocus = true;
        },
        inputBlur: function() {
            var me = this;
            
            me.bInputFocus = false;            
            setTimeout(function () {
                me.restoreSelection();
                me.formatEle({ type: "css", name: "color", value: tinycolor(me.ui.color.minicolors("value")).setAlpha(me.ui.color.minicolors("opacity")).toString() });
            }, 1);
        },
        getSelectionBoundaryElement(isStart) {
            var range, sel, container;
            if (document.selection) {
                range = document.selection.createRange();
                range.collapse(isStart);
                return range.parentElement();
            } else {
                sel = window.getSelection();
                if (sel.getRangeAt) {
                    if (sel.rangeCount > 0) {
                        range = sel.getRangeAt(0);
                    }
                } else {
                    // Old WebKit
                    range = document.createRange();
                    range.setStart(sel.anchorNode, sel.anchorOffset);
                    range.setEnd(sel.focusNode, sel.focusOffset);

                    // Handle the case when the selection was selected backwards (from the end to the start in the document)
                    if (range.collapsed !== sel.isCollapsed) {
                        range.setStart(sel.focusNode, sel.focusOffset);
                        range.setEnd(sel.anchorNode, sel.anchorOffset);
                    }
                }

                if (range) {
                    container = range[isStart ? "startContainer" : "endContainer"];

                    // Check if the container is a text node and return its parent if so
                    return container.nodeType === 3 ? container.parentNode : container;
                }
            }
        },
        updateSetting: function () {
            var me = this;

            this.bUpdatingSettings = true;

            var ele = this.getSelectionBoundaryElement(true),
                oStyle = window.getComputedStyle(ele, null),
                sFontFamily = oStyle.getPropertyValue("font-family"),
                sFontWeight = oStyle.getPropertyValue("font-weight"),
                sFontStyle = oStyle.getPropertyValue("font-style"),
                sTxtDecoration = oStyle.getPropertyValue("text-decoration-line"),
                sColor = oStyle.getPropertyValue("color"),
                sSize = oStyle.getPropertyValue("font-size"),
                sLetterSpacing = oStyle.getPropertyValue("letter-spacing"),
                sLineHeight = oStyle.getPropertyValue("line-height"),
                sTxtTransform = oStyle.getPropertyValue("text-transform");

            var findOne = this.collection.find(function (model) {
                return model.get("PrimaryFont").indexOf(sFontFamily) > -1;
            });

            if (findOne) {
                this.sFontStyleName = findOne.get("PrimaryFontName");
                this.setFontStyleName(findOne.get("FontStyleName"));
            }
            else {
                this.ui.fontWrapper.removeClass("show_font_panel");
            }

            if (sFontWeight === "bold") {
                this.ui.bold.addClass("selected");
            }
            else {
                this.ui.bold.removeClass("selected");
            }

            if (sFontStyle === "italic") {
                this.ui.italic.addClass("selected");
            }
            else {
                this.ui.italic.removeClass("selected");
            }

            if (sTxtDecoration.indexOf("underline") > -1) {
                this.ui.underline.addClass("selected");
            }
            else {
                this.ui.underline.removeClass("selected");
            }

            this.ui.size.data("mospinner").set(parseInt(sSize));
            this.ui.spacing.data("mospinner").set(parseInt(sLetterSpacing));
            this.ui.lineHeight.data("mospinner").set(parseInt(sLineHeight));

            var oColor = tinycolor(sColor);
            me.ui.color.minicolors("value", oColor.toHexString());
            //me.ui.color.minicolors("value", { color: oColor.toHexString(), opacity: oColor.getAlpha() });

            this.ui.textCase.data('selectik').changeCS({ value: sTxtTransform });

            this.bUpdatingSettings = false;
        },
        setFontStyleName: function (sFontStyleName) {
            var me = this;

            this.ui.fontWrapper.addClass("show_font_panel");

            var oView, oChild;
            this.colViewFont.setFilter(function (child, index, collection) {
                if (child.get("PrimaryFontName") === me.sFontStyleName) {
                    oChild = child;
                    child.set("selected", "selected");
                    oView = me.colViewFont.children.findByModel(child);
                    if (oView) {
                        oView.$el.addClass("selected");
                    }
                }
                else {
                    child.set("selected", "");
                    oView = me.colViewFont.children.findByModel(child);
                    if (oView) {
                        oView.$el.removeClass("selected");
                    }
                }
                return child.get('FontStyleName') === sFontStyleName;
            });

            if (oChild) {
                var oFindView = me.colViewFont.children.findByModel(oChild);
                if (oFindView) {
                    me.getRegion("fontList").el.scrollTop = oFindView.$el.position().top;                    
                    //oFindView.el.scrollIntoView({block: "end", behavior: "smooth"});
                }
            }
        },
        onChildviewChangeType: function (view) {
            var me = this;

            this.setFontStyleName(view.model.get("FontStyleName"));
        },
        onChildviewChangeFont: function (view) {
            var me = this;

            me.sFontStyleName = view.model.get("PrimaryFontName");
            setTimeout(function () {
                me.loadFonts(view.model);
            }, 1);

            // request to channel about primaryFont
            var rte = Radio.channel("rte").request("rte:primaryFont", { primaryFont: view.model.get("PrimaryFont") });

            //this.$el.trigger("click");
        },
        showFont: function () {
            this.ui.fontWrapper.removeClass("show_font_panel");
        },
        switchTab: function (event) {
            var arr = ["hide", "hide", "hide"];
            arr[event.currentTarget.getAttribute("tab")] = "show";
            this.getUI("tab1Content")[arr[1]]();
            this.getUI("tab2Content")[arr[2]]();
        },
        clickButton: function (event) {
            var $but = $(event.currentTarget);
            if ($but.hasClass("selected")) {
                $but.removeClass("selected");
            }
            else {
                $but.addClass("selected");
            }
            this.format($(event.currentTarget).attr("type"));
        },
        loadFonts: function (oModel) {
            this.loadFont(oModel.get("PrimaryFont").split("|")[0]);
            this.loadFont(oModel.get("SecondaryFont").split("|")[0]);
            this.loadFont(oModel.get("TertiaryFont").split("|")[0]);

            var sFont = (oModel.get("PrimaryFont").split("|")[1]).split("*")[0];
            this.formatEle({ type: "css", name: "font-family", value: sFont });
        },
        loadFont: function (sTypeKitId) {
            var config = {
                kitId: sTypeKitId,
                scriptTimeout: 3000
            };

            require(["//use.typekit.net/" + sTypeKitId + ".js"], function (something) {
                Typekit.load(config);
            });
        },
        formatTextCase: function (event) {
            var me = this;

            // exit if formatTextCase is called from UpdateSettings function
            if (me.bUpdatingSettings) {
                return;
            }

            setTimeout(function () {
                me.formatEle({ type: "css", name: "text-transform", value: me.ui.textCase.val() });
            }, 1);
        },          
        getSelectedNodes: function() {
            var selectedNodes = [];
            var sel = rangy.getSelection();
            for (var i = 0; i < sel.rangeCount; ++i) {
                selectedNodes = selectedNodes.concat( sel.getRangeAt(i).getNodes() );
            }
            return selectedNodes;
        },
        selectElementContents: function(el) {
            var range = document.createRange();
            range.selectNodeContents(el);
            var sel = window.getSelection();
            sel.removeAllRanges();
            sel.addRange(range);
        },
        format: function (action) {
            var me = this, sClass, sStyle, sValue, bToggle;
            
            //document.execCommand('styleWithCSS', false, true);
            switch (action) {
                case "b":
                    sClass = "bold";
                    sStyle = "font-weight";
                    sValue = "bold";
                    //document.execCommand('bold', false, null);         
                    bToggle = me.ui.bold.hasClass("selected");
                    me.formatEle({type:"class", style:"font-weight", name: "bold", value: "bold", on: bToggle});
                    break;
                case "i":
                    sClass = "italic";
                    sStyle = "font-style";
                    sValue = "italic";
                    bToggle = me.ui.italic.hasClass("selected");
                    //document.execCommand('italic', false, null);
                    me.formatEle({type:"class", style:"font-style", name: "italic", value: "italic", on: bToggle});
                    break;
                case "u":
                    sClass = "underline";
                    sStyle = "text-decoration";
                    sValue = "underline";
                    bToggle = me.ui.underline.hasClass("selected");
                    document.execCommand('underline', false, null);
                    /*
                    me.formatEle({type:"class", style:"text-decoration", name: "underline", value: "underline", 
                                  on: bToggle});            
                    */
                    break;
            }

            var arrNodes = me.getSelectedNodes();   

            //if (action === "b") {
                //var $wrapper = $("<span id='tmpid' class='" + sClass + "'>");
                //$(arrNodes[0].parentElement).contents().unwrap().wrap($wrapper);
                //this.selectElementContents($("#tmpid").prop("id","")[0]);
            //}
            
            //console.log( arrNodes[0].parentElement );
            //$(arrNodes[0].parentElement).contents().unwrap().wrap($wrapper);
            
            /*
            _.each(arrNodes, function(val, key, list){                        
                // if it's element
                if (val.nodeType === 1) {
                    $ele = $(val);                   
                    if (val.tagName === action.toUpperCase()) {
                        
                    }                    
                }
            });
            */
            
            /*
            var arrNodes = me.getSelectedNodes(), $ele;
            
            // replace style with class in parentElement
            $ele = $(arrNodes[0].parentElement);            
            if ( $ele[0].style[sStyle].indexOf(sValue) > -1 ) {         
                $ele.css(sStyle, "").addClass(sClass);        
            }
            else {
                $ele.removeClass(sClass); 
            }

            // loop childNodes to replace style with class in parentElement
            _.each(arrNodes, function(val, key, list){                        
                // if it's element
                if (val.nodeType === 1) {
                    $ele = $(val);                   

                    if ( $ele[0].style[sStyle].indexOf(sValue) > -1 ) {         
                        $ele.css(sStyle, "").addClass(sClass);        
                    }
                    else {
                        $ele.removeClass(sClass);    
                    }
                }
            });
            */
            
            this.saveSelection();
        },
        formatEle: function (oParam) {
            if (window.getSelection().focusNode === null) return;

            document.execCommand('hiliteColor', false, "rgba(123,123,123,0.1)");
        
            var channel = Radio.channel("rte");
            // request for content editable element
            var rte = channel.request("rte:rteEle", {});

            var find = $('*', rte.rteEle).filter(function () {
                var match = 'rgba(123, 123, 123';

                return $(this).css('background-color').startsWith(match);
            });

            if (find.length > 0) {
                // remove background
                var obj = { 'background-color': '' };

                if (oParam.type === "css") {
                    // add css
                    obj[oParam.name] = oParam.value;
                    find.css(obj);

                    // empty children style in each element
                    find.each(function () {
                        var obj2 = {};
                        obj2[oParam.name] = '';
                        $("*", this).css(obj2);
                    });
                }
                // obselete
                else if (oParam.type === "class") {
                    var arrNodes = this.getSelectedNodes(), $ele;
        
                    // loop childNodes to remove classes
                    _.each(arrNodes, function(val, key, list){                        
                        // if it's element
                        if (val.nodeType === 1) {
                            $ele = $(val);                   
                            $ele.removeClass(oParam.name)
                                .removeClass("no-" + oParam.name);
                        }
                    });

                    // remove background-color
                    find.css(obj);

                    if (oParam.on) {
                        find.addClass(oParam.name)
                            .removeClass("no-" + oParam.name);
                    }
                    else {
                        find.removeClass(oParam.name)
                            .addClass("no-" + oParam.name);
                    }
                    
                    /*
                    if (($(find[0]).css(oParam.style)+"").indexOf(oParam.value) > -1) {
                        find.removeClass(oParam.name)
                            .addClass("no-" + oParam.name);
                    }   
                    else {
                        find.removeClass("no-" + oParam.name)
                            .addClass(oParam.name);   
                    } 
                    */

                    // empty children style in each element
                    /*
                    find.each(function () {
                        $("*", this).removeClass(oParam.name).removeClass("no-" + oParam.name);
                    });
                    */
                    
                    /*
                    var arrNodes = this.getSelectedNodes(), $ele;
        
                    // loop childNodes to replace style with class in parentElement
                    _.each(arrNodes, function(val, key, list){                        
                        // if it's element
                        if (val.nodeType === 1 && val.getAttribute("dontchange") !== "1") {
                            $ele = $(val);                   
                            $ele.removeClass(oParam.name).removeClass("no-" + oParam.name);
                        }
                    });
                    */
                    
                    //find.css(obj).attr("dontchange", "0");
                }
            }
            
            this.saveSelection();
        },
        saveSelection: function () {
            var channel = Radio.channel("rte"),
                rte = channel.request("rte:saveSelection", {}),
                rte2 = channel.request("rte:rteEle", {});

            // trigger rte:edit so listen is save the changes
            channel.trigger("rte:edit", { ele: rte2.rteEle });
        },
        restoreSelection: function () {
            var channel = Radio.channel("rte");

            var rte = channel.request("rte:restoreSelection", {});
        },
        removeFormat: function () {
            var channel = Radio.channel("rte"),
                rte2 = channel.request("rte:rteEle", {});

            channel.trigger("rte:removeFormat", { ele: rte2.rteEle });
        }
    });

    return view;
});