'use strict';

define(['jquery', 'underscore', 'backbone', 'marionette', 'backbone.radio' , 'view/item'], 
function($, _, Bb, Mn, Radio, ItemView){
    var view = Mn.CollectionView.extend({
        tagName: "ul",
        //collection: new Bb.Collection(),
        childView: ItemView,        
        onRender: function() {
            console.log('ul - render');
        },
        onDestroy: function() {
            console.log('ul - destroy');
        },
    });
    
    return view;
});
