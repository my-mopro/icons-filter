'use strict';

define("rtechannel", ['jquery', 'underscore', 'backbone', 'marionette', 'backbone.radio', 'rtepanel'], 
/* 
    Warning css is removed, search for css, Styler
*/
function($, _, Bb, Mn, Radio, RtePanelView){
    var view = Mn.View.extend({
        el: document,
        template: false,
        defaultOptions: {
            // allow edit for certain selectors
            allowEditSelectors: ".js-aieditor-title, .js-aieditor-subtitle, .js-aieditor-vptitle, .js-aieditor-vpsubtitle, .js-aieditor-name, .js-aieditor-role"
        },
        events: function() {
            var oRet = { 
                "mousedown": function(event) { this.focusOnRte = false; this.mouseDownElement = event.target; },
                "mouseup": function(event) { this.selectionChange(); },
                "click": function(event) { this.clickDoc(event); }, 
                "mousedown .js_rte": function(event) { this.focusOnRte = true; },
                "mouseup .js_rte": function(event) { this.clickRtr(event); },
                "click .js_rte": function(event) { event.stopPropagation(); },                                
                "keyup": function(event) { this.selectionChange(); },
                /*
                "keydown": function (event) {
                    // I need to use keydown with setTimeout coz keyup event is stopped for some reason
                    setTimeout(function () {
                        me.selectionChange();
                    }, 500);
                },
                */
            };            
            
            oRet["mousedown " + this.defaultOptions.allowEditSelectors] = function(event) { this.mouseDownEditableElement = event.currentTarget; } ; 
            oRet["mouseup " + this.defaultOptions.allowEditSelectors] = function(event) { this.clickEditable(event); } ;
            oRet["click " + this.defaultOptions.allowEditSelectors] = function(event) { event.stopPropagation() };
                        
            return oRet;
        },
        initialize: function(){
            var me = this;
            
            me.options = _.extend({}, me.defaultOptions, me.options);

            me.StylerCodes = [];
            // store current rte element
            this.rteELe = null; 
            // store current target - rte element or doc
            this.currentTarget = this.el;  
            // initial rte panel
            this.rtePanel = new RtePanelView({collection: this.collection}).render();
            this.$el.find("body").append(this.rtePanel.el);
            
            // buid stylerCodes array
            me.collection.each(function (oModel) {
                me.StylerCodes.push(oModel.get("StylerCode").replace(/[\[\]']+/g, ''));
            });
            
            // reply current content editable element
            var channel = Radio.channel("rte");             
                                    
            channel.reply("rte:rteEle", function(oParam) { 
                return {rteEle: me.rteEle}; 
            });
            
            channel.reply("rte:saveSelection", function(oParam) { 
                me.saveSel(); 
            });
            
            channel.reply("rte:restoreSelection", function(oParam) { 
                me.restoreSel(); 
            });
            
            channel.reply("rte:primaryFont", function (oParam) {           
                return;
                me.Styler = new css.Styler();

                var CSSCollection = me.options.cssRowCol;
                if (CSSCollection.size() > 0) {
                    var CSSModel = CSSCollection.at(0);    //Normally, it should be only one model

                    if (typeof (me.Styler) != undefined && me.Styler != null && me.Styler != '') {
                        me.Styler.loadStyler("[" + me.StylerCodes + "]");//new Backbone.Collection()
                        me.Styler.loadFonts(new Backbone.Collection(CSSModel.get("FontMap").rows), CSSModel.get("CSSID"), false);
                        me.Styler.options.SourceID = CSSModel.get("CSSID");
                        var varKitID = oParam.primaryFont.split("|")[0];
                        var varCSS = oParam.primaryFont.split("|")[1];
                        if (varKitID != "inherit") {
                            me.Styler.addFont(varKitID, varCSS.split("*")[0]);
                            me.Styler.addFont(varKitID, varCSS.split("*")[1]);
                            setTimeout(function () {
                                me.Styler.saveFonts();
                            }, 1000);

                        }
                    }

                }
            });
            
            channel.reply("rte:selectAll", function(oParam) { 
                if (me.rteEle) {
                    me.selectAll();    
                    
                    setTimeout(function(){
                        me.mouseDownElement = me.rteEle;
                        me.currentTarget = me.rteEle;
                        me.selectionChange();
                    }, 10);
                }
            });
            
            console.log('rte channel - init');
        },        
        onRender: function() {
            console.log('rte channel - render');
        },
        onDestroy: function() {
            console.log('rte channel - destroy');
        },
        destroyMe: function() {
            this.rtePanel.destroy();
            // trigger stopListening to destroy listners                    
            Radio.channel("rte").trigger("rte:stopListening");                   
            this.destroy();            
            this.undelegateEvents();
            this.remove();        
        },      
        IsFocusOnRte: function() {
            return !!$(this.mouseDownElement).closest(".js_rte").length;
        },
        selectAll: function() {
            var me = this,
                objRange = document.createRange(),
                objSelection = window.getSelection();

            objRange.selectNodeContents(me.rteEle);
            objSelection.removeAllRanges();
            objSelection.addRange(objRange);  
        },
        selectionChange: function() { 
            if (this.IsFocusOnRte()) {
            }
            else {
                if (this.isSelectable()) {
                    if (this.saveSel()) {
                        this.rtePanel.updateSetting();
                        this.positionPanel();
                    }
                    else {
                        this.rtePanel.$el.hide();
                    }
                }                                    
                else {
                    this.rtePanel.$el.hide();
                }
            }            
        },
        isSelectable: function() {       
            return !!$(this.mouseDownElement).closest(this.options.allowEditSelectors).length;
        },
        calPosition: function(oPosition) {
            var iTop, iLeft, 
                $win = $(window), 
                iPanelWidth = this.rtePanel.$el.outerWidth(), 
                oRet = {top: 0, left: '', right: ''};
            
            iTop = oPosition.top + $(window).scrollTop();        
            // determine to show panel on top/bottom of selected text area 
            // show on top if top is above half of window height
            if ( $win.height()/2 < oPosition.top ) {
                iTop -= (this.rtePanel.$el.outerHeight() + 5);
            }
            else {
                iTop += oPosition.height + 5;   
            }            
            oRet.top = iTop;
            
            iLeft = oPosition.left + oPosition.width/2 - iPanelWidth/2;
            if (iLeft < 0) {
                oRet.left = 10;
            }
            else if (iLeft+iPanelWidth > $win.width()) {
                 oRet.right = 10;
             }
            else {
                oRet.left = iLeft;
            }
            
             return oRet;
        }, 
        positionPanel: function() {            
            this.rtePanel.$el.show().css(this.calPosition(this.savedRange.getBoundingClientRect()));            
        },
        saveSel: function() {
            var range;
                                                
            //try {
                if(window.getSelection)//non IE Browsers
                {
                    range = window.getSelection().getRangeAt(0);
                }
                else if(document.selection)//IE
                { 
                    range = document.selection.createRange();  
                }                 
            
                if ( range.toString().length > 0 ) {
                    // save Range
                    this.savedRange = range.cloneRange();                    
                    return true;
                }
                else {
                    return false;
                }
            //catch(e) {
              //  ;
            //}
        },
        //to restore sel
        restoreSel: function(){
            if (this.savedRange) {                
                if (window.getSelection)//non IE and there is already a selection
                {
                    var s = window.getSelection();
                    if (s.rangeCount > 0) 
                        s.removeAllRanges();
                    s.addRange(this.savedRange);
                }
                else if (document.createRange)//non IE and no selection
                {
                    window.getSelection().addRange(this.savedRange);
                }
                else if (document.selection)//IE
                {
                    this.savedRange.select();
                }
            }
        },
        clickDoc: function(event) {            
            // trigger leave since doc is clicked
            var channel = Radio.channel("rte");    

            if (this.rteEle && event.currentTarget !== this.currentTarget) {
                channel.trigger("rte:leave", {ele: this.rteEle});   
            }
            // use currentTarget coz rteEle cannot be set to null otherwise listener won't be able to access it
            // currentTarget is use in clickEditable to check to previous click element is doc or rte element
            this.currentTarget = event.currentTarget;            
        },
        clickEditable: function(event) {            
            var channel = Radio.channel("rte");    
            
            if (this.currentTarget !== document && this.rteEle !== this.mouseDownEditableElement) {            
                channel.trigger("rte:leave", {ele: this.rteEle});    
            }
            
            //if click rte 1, then click doc and click rte 1 again, rteEle and event.currentTarget will still be the same
            //so it has to compare with this.currentTarget coz the previous click element could be doc element
            if (this.rteEle !== this.mouseDownEditableElement || this.rteEle !== this.currentTarget) { 
                // trigger focuse on content editable element
                this.currentTarget = this.rteEle = this.mouseDownEditableElement;                        
                channel.trigger("rte:enter", {ele: this.rteEle});
            }        
        },
        clickRtr: function(event) {                  
            var bRestore = true;

            // restore selection if click element other than input 
            switch(event.target.tagName) {
                case "SELECT":
                    bRestore = false;
                    break;
                case "BUTTON":
                    bRestore = false;
                    break;
                case "INPUT":
                    bRestore = false;
                    break;    
            }

            // click on selectik
            if ($(event.target).hasClass("custom-text")) {
                bRestore = false;
            }
            
            if (bRestore) {
                this.restoreSel();
            }                    
        }
    });

    return view;
});
