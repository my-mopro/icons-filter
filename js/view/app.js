'use strict';

define(['jquery', 'underscore', 'backbone', 'marionette', 'view/header', 'view/main', 'rtechannel', 'molock', 'mospinner'],
function($, _, Bb, Mn, HeaderView, MainView, RteChannel) {
    var view = Mn.View.extend({
        el: "#containerRegion",  
        template: false,
        regions: {
            header: "#headerRegion",
            main: "#mainRegion"
        },
        initialize: function() {                        
            this.showChildView("header", new HeaderView());
        },
        onRender: function() {            
        }
        
    });
    
    var view3 = Mn.View.extend({
        template: _.template("ghi"),        
        /*
        regions: {
            header: "#headerRegion",
            main: "#mainRegion"
        },
        initialize: function() {                        
            this.showChildView("header", new HeaderView());
        },
        */
        onRender: function() {            
        }
        
    });
    
    var view2 = Mn.View.extend({
        template: _.template("def <div class='d3'></div>"),                
        regions: {
            d3: ".d3"
        },
        initialize: function() {                                    
        },        
        onRender: function() {            
            this.showChildView("d3", new view3());
        }
        
    });
    
    var view1 = Mn.View.extend({
        el: "#d1",
        //template: _.template("abc <div class='d2'></div>"),        
        template: _.template("abc"),        
        regions: {
            d2: ".d2"
        },
        initialize: function() {                                    
        },
        onRender: function() {      
            this.$el.append("<div class='d2'></div>");
            this.addRegion("d2", ".d2");
            this.showChildView("d2", new view2());    
        }
        
    });
    
    new view1().render();

    return view;
});